import React from 'react';
import './menu.css'

const Menu = ({menuList, addItem}) => {

    const createMenuItemsComponents = (Arr) => {

        const allItems = [];

        for (let i = 0; i < Arr.length; i ++) {
            let item =
                <li className="menu_item"
                    key= {i}
                    onClick= {() => addItem(Arr[i].name, Arr[i].price)}>
                <div className="menu_item_img">
                    <p>{Arr[i].img}</p>
                </div>
                <div className="menu_item_info">
                    <p className="menu_item_name">{Arr[i].name}</p>
                    <p className="menu_item_price">Price: {Arr[i].price}</p>
                </div>
            </li>

            allItems.push(item);
        }

        return allItems;
    }


    return (
        <div className="menu_block">
            <div className="menu_block_inner">
                <h3 className="menu_block_title">Add items</h3>
                <ul className="menu_list">
                    {createMenuItemsComponents(menuList)}
                </ul>
            </div>
        </div>
    );
};

export default Menu;