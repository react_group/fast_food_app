import React from 'react';
import './order.css'

const Order = ({orderList, totalPrice, deleteItem, deleteAllOrders}) => {

    const createOrderListComponents = (Arr) => {
        const allOrders = [];

        for (let i = 0; i < Arr.length; i++) {
            let order =
            <li className="order_list_item"
                key={i}
            >
                <p className="item_name">{Arr[i].name}</p>
                <p className="item_quantity">{Arr[i].quantity}</p>
                <p className="item_">price: {Arr[i].price}</p>
                <button type="button" className="item_delete"
                        onClick={() => deleteItem(Arr[i].name)}
                >X</button>
            </li>

            allOrders.push(order);
        }

        return allOrders;
    }


    return (
        <div className="order_block">
            <div className="order_block_inner">
                <h3 className="order_block_title">Order details</h3>
                <ul className="order_list">
                    {createOrderListComponents(orderList)}
                </ul>

                <div className="order_total-price">
                    <p>Total price: {totalPrice}</p>
                    <button type='button'
                            onClick={deleteAllOrders}
                    >Clear your cart</button>
                </div>
            </div>

        </div>
    );
};

export default Order;