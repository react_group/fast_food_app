import logo from './logo.svg';
import './App.css';
import Order from "./components/order/order";
import Menu from "./components/menu/menu";
import {useState} from "react";

function App() {

    const [menu, setMenu] = useState([
        {name: 'Whisky', price: 180, img: <i className="fas fa-glass-whiskey"></i>},
        {name: 'Beer', price: 180, img: <i className="fas fa-beer"></i>},
        {name: 'Vodka', price: 180, img: <i className="fas fa-cocktail"></i>},
        {name: 'Rum', price: 180, img: <i className="fas fa-cocktail"></i>},
        {name: 'Cola', price: 180, img: <i className="fas fa-wine-glass"></i>},
        {name: 'Red-Bul', price: 180, img: <i className="fas fa-wine-bottle"></i>},
        {name: 'Tonik', price: 180, img: <i className="fas fa-glass-cheers"></i>}
    ]);

    const [orderList, setOrderList] = useState([
        {name: 'Whisky', quantity: 1, price: 180}
    ]);


    const addItem = (name, price) => {
        const newOrderList = orderList.map(o => (o.name));
        if (newOrderList.includes(name)) {
            setOrderList(orderList.map(o => {
                if (o.name === name) {
                    return {...o, quantity: o.quantity+1, price: price * (o.quantity + 1)}
                }
                return o;
            }));
        } else {
            let order = {
                name: name,
                quantity: 1,
                price: price
            }
            let newOrderList = [...orderList, order];
            setOrderList(newOrderList);
        }
    }


    const getTotalPrice = () => {
        const newOrderList = orderList.map(o => (o.price));
        let sum = 0;
        for (let i = 0; i < newOrderList.length; i++) {
            sum += newOrderList[i]
        }
        return sum;
    }

    const deleteItem = (name) => {
        const newOrderList = orderList.map(o => (o.name));
        let index = newOrderList.indexOf(name);
        setOrderList(orderList.filter(order => orderList.indexOf(order) !== index));
    }


    const deleteAllOrders = () => {
        setOrderList([]);
    }


  return (
      <div className="container">
          <h1 className="App_title">The Faster Bar in your city!</h1>
          <div className="App">
              <Order
                  orderList = {orderList}
                  totalPrice = {getTotalPrice()}
                  deleteItem = {(name) => deleteItem(name)}
                  deleteAllOrders = {() => deleteAllOrders()}
              ></Order>

              <Menu
              menuList = {menu}
              addItem = {(name, price) => addItem(name, price)}
              ></Menu>

          </div>
      </div>
  );
}

export default App;
